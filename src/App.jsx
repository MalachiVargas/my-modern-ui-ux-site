import {
  Navbar,
  Billing,
  CardDeal,
  Business,
  Clients,
  CTA,
  Stats,
  Footer,
  Testimonials,
  Hero
} from './components/index'

const App = () => (
  <div className="overflow-hidden bg-primary">
    <div className="px-6 sm:px-16">
      <Navbar />
    </div>

    <Hero />
    <div className="px-6 sm:px-16">
      <Stats />
      <Business />
      <Billing />
      <CardDeal />
      <Testimonials />
      <Clients />
      <CTA />
      <Footer />
    </div>
  </div>
)

export default App
