import React from 'react'
import { logo } from '../assets'
import { footerLinks, socialMedia } from '../constants'

const Footer = () => (
  <section className="mt-10 grid auto-rows-auto py-6 sm:py-16 ">
    <div className="flex flex-col md:flex-row">
      <div>
        <img src={logo} alt="hoobank" className="h-[75px] w-[275px]" />
        <p className="mt-5 max-w-[600px] font-poppins text-[18px] font-normal leading-[30.8px] text-dimWhite">
          A new way to make the payments easy, reliable and secure.
        </p>
      </div>
      <div className="mb-6 mt-6 flex w-full flex-wrap justify-start gap-x-6 md:mt-0 md:justify-evenly">
        {footerLinks.map((flink) => (
          <div key={flink.title} className="flex flex-col py-5">
            <h4 className="mb-4 font-poppins text-[18px] font-medium leading-[27px] text-white">
              {flink.title}
            </h4>
            <ul className="list-none space-y-4">
              {flink.links.map((alink) => (
                <li
                  className="mb-4 cursor-pointer font-poppins text-[16px] font-normal leading-[24px] text-dimWhite hover:text-secondary"
                  key={alink.name}
                >
                  <a href={alink.link}>{alink.name}</a>
                </li>
              ))}
            </ul>
          </div>
        ))}
      </div>
    </div>
    <div className="flex w-full flex-col items-center justify-between border-t-[1px] border-t-[#3F3E45] pt-6 md:flex-row">
      <p className="text-center font-poppins text-[18px] font-normal leading-[27px] text-white">
        Copyright Ⓒ 2022 HooBank. All Rights Reserved.
      </p>
      <div className="mt-6 flex gap-x-6 md:mt-0">
        {socialMedia.map((sm) => (
          <a key={sm.id} href={sm.link}>
            <img src={sm.icon} alt={sm.id} />
          </a>
        ))}
      </div>
    </div>
  </section>
)

export default Footer
