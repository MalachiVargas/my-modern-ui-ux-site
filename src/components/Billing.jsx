import { apple, bill, google } from '../assets'

const Billing = () => {
  return (
    <section
      id="product"
      className="mb-6 grid grid-cols-[repeat(auto-fit,_minmax(min(515px,_100%),_1fr))] gap-x-10 py-6 sm:py-10"
    >
      <div className="relative order-2 flex justify-center lg:order-1">
        <img className="z-[5] h-full w-full" src={bill} alt="billing" />
        <div className="white__gradient absolute -left-1/2 top-0 z-[3] h-[50%] w-[50%] rounded-full" />
        <div className="pink__gradient absolute -left-1/2 bottom-0 z-[0] h-[50%] w-[50%] rounded-full" />
      </div>
      <div className="order-1 flex flex-col justify-center lg:order-2">
        <h2 className="max-w-[515px] font-poppins text-[40px] font-semibold leading-[66.8px] text-white xs:text-[48px] xs:leading-[76.8px]">
          {`Easily control your billing & invoicing`}
        </h2>
        <p className="mt-5 max-w-[500px] font-poppins text-[18px] font-normal leading-[30.8px] text-dimWhite md:text-justify md:tracking-[-0.1px]">
          Send money securely around the world. Pay later nearly everywhere you
          shop. Support causes that matter to you. Explore crypto. Who knew one
          app could be so mighty?
        </p>
        <div className="mt-5 flex gap-x-5">
          <a href="https://apps.apple.com">
            <img src={apple} alt="app store" />
          </a>
          <a href="https://play.google.com">
            <img src={google} alt="play store" />
          </a>
        </div>
      </div>
    </section>
  )
}

export default Billing
