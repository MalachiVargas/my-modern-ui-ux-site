import React from 'react'

const CTA = () => (
  <section className="bg-black-gradient-2 box-shadow mt-12 flex flex-wrap items-center justify-center rounded-[20px] p-8 sm:justify-between">
    <div className="flex flex-col items-center justify-center sm:justify-start">
      <h2 className="max-w-[600px] font-poppins text-[40px] font-semibold leading-[66.8px] text-white xs:text-[48px] xs:leading-[76.8px]">
        Let's try our service now!
      </h2>
      <p className="mt-5 max-w-[600px] font-poppins text-[18px] font-normal leading-[30.8px] text-dimWhite">
        Everything you need to accept card payments and grow your business
        anywhere on the planet.
      </p>
    </div>
    <button className="bg-blue-gradient mt-6 rounded-[10px] py-4 px-6 font-poppins text-[18px] font-medium outline-none">
      Get Started
    </button>
  </section>
)

export default CTA
