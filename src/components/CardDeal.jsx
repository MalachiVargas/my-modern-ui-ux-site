import { card } from '../assets'

const CardDeal = () => {
  return (
    <section className="mb-6 grid grid-cols-[repeat(auto-fit,_minmax(min(515px,_100%),_1fr))] gap-x-10 py-6 sm:py-10">
      <div className="flex flex-col items-start justify-center">
        <h2 className="max-w-[600px] font-poppins text-[40px] font-semibold leading-[66.8px] text-white xs:text-[48px] xs:leading-[76.8px]">
          Find a better card deal in few easy steps.
        </h2>
        <p className="mt-5 max-w-[570px] font-poppins text-[18px] font-normal leading-[30.8px] text-dimWhite">
          Explore our line of credit and card options. Discover our debit card,
          get cash back with the bank credit card, and finance purchases with
          Bank Credit.
        </p>
        <button className="bg-blue-gradient mt-6 rounded-[10px] py-4 px-6 font-poppins text-[18px] font-medium outline-none">
          Get Started
        </button>
      </div>
      <div>
        <img className="h-full w-full" src={card} alt="card deal" />
      </div>
    </section>
  )
}

export default CardDeal
