import { clients } from '../constants'

const Clients = () => (
  <section className="flex flex-wrap items-center justify-evenly gap-x-4">
    {clients.map((client) => (
      <img
        className="h-[100px] w-[100px] object-contain sm:w-[200px]"
        key={client.id}
        src={client.logo}
        alt={client.id}
      />
    ))}
  </section>
)

export default Clients
