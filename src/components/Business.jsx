import { features } from '../constants'

const Business = () => {
  return (
    <section
      id="features"
      className="grid grid-cols-[repeat(auto-fit,_minmax(min(515px,_100%),_1fr))] gap-x-10 py-6 sm:py-16"
    >
      <div className="flex flex-col items-start ">
        <h2 className="max-w-[600px] font-poppins text-[40px] font-semibold leading-[66.8px] text-white xs:text-[48px] xs:leading-[76.8px]">
          You do the business, we'll handle the money.
        </h2>
        <p className="mt-5 max-w-[600px] font-poppins text-[18px] font-normal leading-[30.8px] text-dimWhite">
          With the right credit card, you can improve your financial life by
          building credit, earning rewards and saving money. But with hundreds
          of credit cards on the market.
        </p>
        <button className="bg-blue-gradient mt-6 rounded-[10px] py-4 px-6 font-poppins text-[18px] font-medium outline-none">
          Get Started
        </button>
      </div>
      <div className="mt-10 grid grid-rows-3 justify-items-end gap-y-4 md:mt-0">
        {features.map((feature) => (
          <div
            key={feature.id}
            className="feature-card flex items-center justify-center rounded-[20px] p-6"
          >
            <div className="flex h-[65px] w-[65px] flex-shrink-0 items-center justify-center rounded-full bg-dimBlue">
              <img
                className="h-[50%] w-[50%] object-contain"
                src={feature.icon}
                alt={feature.id}
              />
            </div>
            <div className="ml-3 font-poppins">
              <h3 className="mb-1 text-[18px] font-semibold text-white">
                {feature.title}
              </h3>
              <p className="max-w-[620px] text-[16px] font-normal leading-[22px] text-dimWhite">
                {feature.content}
              </p>
            </div>
          </div>
        ))}
      </div>
    </section>
  )
}

export default Business
