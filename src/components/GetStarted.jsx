import styles from '../styles'
import { arrowUp } from '../assets'

const GetStarted = () => {
  return (
    <div className="bg-blue-gradient h-[140px] w-[140px] cursor-pointer rounded-full p-[2px]">
      <div className="flex h-[100%] w-[100%] flex-col items-center justify-center rounded-full bg-primary">
        <div className="flex items-start justify-center">
          <p className="mr-2 font-poppins text-[18px] font-medium leading-[23px]">
            <span className="text-gradient">Get</span>
          </p>
          <img
            src={arrowUp}
            alt="arrow up"
            className="h-[23px] w-[23px] object-contain"
          />
        </div>
        <p className="font-poppins text-[18px] font-medium leading-[23px]">
          <span className="text-gradient">Started</span>
        </p>
      </div>
    </div>
  )
}

export default GetStarted
