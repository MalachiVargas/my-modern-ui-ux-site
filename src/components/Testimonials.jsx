import React from 'react'
import { quotes } from '../assets'
import { feedback } from '../constants'

const Testimonials = () => {
  return (
    <section id="clients" className="relative py-6 sm:py-10">
      <div className="blue__gradient absolute -right-[50%] top-40 z-[0] h-[75%] w-[60%] rounded-full" />
      <div className="z-[1] mb-6 flex flex-wrap justify-evenly">
        <h2 className="max-w-[600px] font-poppins text-[40px] font-semibold leading-[66.8px] text-white xs:text-[48px] xs:leading-[76.8px]">
          What People are saying about us
        </h2>
        <p className="mt-5 max-w-[570px] font-poppins text-[18px] font-normal leading-[30.8px] text-dimWhite">
          Everything you need to accept card payments and grow your business
          anywhere on the planet.
        </p>
      </div>
      <div className="feedback-container z-[1] flex flex-wrap justify-center gap-x-6">
        {feedback.map((fb) => (
          <div
            key={fb.id}
            className="feedback-card flex max-w-[400px] flex-col justify-between rounded-[14px] px-10 py-12"
          >
            <img className="h-[50px] w-[50px]" src={quotes} alt="quote marks" />
            <p className="mt-5 max-w-[300px] py-6 font-poppins text-[18px] font-normal leading-[30.8px] text-white">
              {fb.content}
            </p>
            <div className="flex items-end">
              <img className="h-[60px] w-[60px]" src={fb.img} alt={fb.id} />
              <div className="ml-3 font-poppins">
                <h3 className="mb-1 text-[18px] font-semibold text-white">
                  {fb.name}
                </h3>
                <p className="text-[16px] font-normal leading-[22px] text-dimWhite">
                  {fb.title}
                </p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </section>
  )
}

export default Testimonials
