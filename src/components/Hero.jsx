import styles from '../styles'
import { discount, robot } from '../assets'
import GetStarted from './GetStarted'

const Hero = () => {
  return (
    <section
      id="home"
      className="grid grid-cols-[repeat(auto-fit,_minmax(min(515px,_100%),_1fr))] gap-x-10 py-6 sm:py-16"
    >
      <div className="flex flex-1 flex-col items-start justify-center px-6 sm:px-16">
        <div className="bg-discount-gradient mb-2 flex rounded-[10px] py-[6px] px-4 first-line:items-center">
          <img src={discount} alt="discount" className="h-[32px] w-[32px]" />
          <p className="ml-2 font-poppins text-[18px] font-normal leading-[30.8px] text-dimWhite">
            <span className="text-white">20% </span>
            Discount For <span className="text-white">1 Month </span>
            Account
          </p>
        </div>
        <div className="flex items-center gap-x-10 pr-7">
          <h1 className="font-poppins text-[52px] font-semibold leading-[75px] text-white ss:text-[72px] ss:leading-[100px]">
            The Next
            <br className="hidden sm:block" />{' '}
            <span className="text-gradient">Generation</span>
          </h1>
          <div className="hidden ss:flex">
            <GetStarted />
          </div>
        </div>

        <h1 className="font-poppins text-[52px] font-semibold leading-[75px] text-white ss:text-[68px] ss:leading-[100px]">
          Payment Method.
        </h1>
        <p className="mt-5 max-w-[470px] pr-6 font-poppins text-[18px] font-normal leading-[30.8px] text-dimWhite">
          Our team of experts uses a methodology to identify the credit cards
          most likely to fit your needs. We examine annual percentage rates,
          annual fees.
        </p>
      </div>
      <div className="relative my-10 flex flex-1 justify-center md:my-0">
        <img src={robot} alt="billing" className="z-[5] h-full w-full" />

        <div className="pink__gradient absolute top-0 z-[0] h-[35%] w-[40%]" />
        <div className="white__gradient absolute bottom-40 z-[1] h-[80%] w-[80%] rounded-full" />
        <div className="blue__gradient absolute right-20 bottom-20 z-[0] h-[50%] w-[50%]" />
      </div>
      <div className="justify-self-center ss:hidden">
        <GetStarted />
      </div>
    </section>
  )
}

export default Hero
